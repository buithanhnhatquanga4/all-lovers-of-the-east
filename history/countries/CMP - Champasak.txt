capital = 790

oob = “CMP_1”

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 2
}

set_convoys = 0

add_ideas = {
religion_secular
}

set_politics = {
	ruling_party = weixinism
	last_election = "1889.5.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    parishadism = 0
	revolutionary_datongism = 0
	datongism = 0
	weixinism = 100
	xiaoism = 0
	faquanism = 0
	mingism = 0
    zhongism = 0
}