﻿capital = 295

oob = "CON"

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 1
}

set_convoys = 5

set_politics = {
	ruling_party = faquanism
	last_election = "1889.5.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	parishadism = 4
	revolutionary_datongism = 4
	datongism = 4
	weixinism = 4
	xiaoism = 9
	faquanism = 44
	mingism = 12
	zhongism = 19
}

create_country_leader = {
	
	name = "Nguyen Phuc Hong Hung"
	picture = "gfx/leaders/Africa/africaman.tga"
	expire = "1965.1.1"
	ideology = faquanism_subtype
	traits = {
		
	}
}
