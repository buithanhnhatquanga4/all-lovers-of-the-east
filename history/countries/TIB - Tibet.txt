﻿capital = 757

oob = ""

set_politics = {
	ruling_party = faquanism
	last_election = "1889.5.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	parishadism = 4
	revolutionary_datongism = 4
	datongism = 4
	weixinism = 4
	xiaoism = 9
	faquanism = 44
	mingism = 12
	zhongism = 19
}

create_country_leader = {
	name = ""
	desc = "POLITICS__DESC"
	picture = ""
	expire = "1953.3.1"
	ideology = faquanism_subtype
	traits = {
		
	}
}
}
