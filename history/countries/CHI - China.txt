﻿capital = 805 # Ye

oob = ""

set_stability = 0.2
set_war_support = 0.5

create_faction = "Great Continental Order"
add_to_faction= TAI
add_to_faction= SIK

if = {
	limit = {
		OR = {
			has_dlc = "Together for Victory"
			has_dlc = "Man the Guns"
		}
	}
	set_autonomy = {
		target = TAI
		target = SIK
		autonomous_state = autonomy_integrated_puppet
	}
	else = {
		puppet = TAI
		puppet = SIK
	}
}

add_ideas = {
	
}

set_convoys = 40

set_politics = {
	ruling_party = faquanism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	parishadism = 3
	revolutionary_datongism = 2
	datongism = 4
	weixinism = 3
	xiaoism = 5
	faquanism = 43
	mingism = 17
	zhongism = 23
}

create_country_leader = {
	name = "Dragon Council of Military Governors"
	desc = "POLITICS__DESC"
	picture = "gfx/leaders/ASIA/CHI/Dragon_Council.tga"
	expire = "1953.3.1"
	ideology = faquanism_subtype
	traits = {
		
	}
}


