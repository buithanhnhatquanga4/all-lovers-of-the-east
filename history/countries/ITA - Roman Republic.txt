capital = 113

oob = “IRE_1”

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 2
}

set_convoys = 0

add_ideas = {
religion_secular
}

set_politics = {
	ruling_party = faquanism
	last_election = "1889.5.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    parishadism = 1
	revolutionary_datongism = 2
	datongism = 13
	weixinism = 12
	xiaoism = 13
	faquanism = 53
	mingism = 3
    zhongism = 3
}

create_country_leader = {
	name = "Árd Rí Séamus Ó’Néill"
	picture = "/gfx/leaders/IRE/Portrait_IRE_Ard_Ri_Seamus_Oneill.tga"
	expire = "1965.1.1"
	ideology = faquanism_subtype
	traits = {

	}
}