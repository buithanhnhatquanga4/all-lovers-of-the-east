name="!Dev Build - All Lovers of the East"
path="mod/all-lovers-of-the-east"
version="1.8.*"
tags={
	"Alternative History"
	"National Focuses"
	"Ideologies"
	"Graphics"
}

replace_path = "common/ideologies"
replace_path = "history/countries"
replace_path = "history/states"
replace_path = "common/countries"
replace_path = "common/country_tags"
replace_path = "gfx/flags"
replace_path = "gfx/leaders"