ideas = {
	country = {
		AMS_prospering_city = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea AMS_prospering_city"}
			picture = AMS_city_wealth
			modifier = {
				political_power_gain = +0.1
				production_speed_buildings_factor = 0.1
				local_building_slots_factor = 0.1
			}
		}
		AMS_northern_swan = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea AMS_northern_swan"}
			picture = AMS_northern_swan
			modifier = {
				political_power_gain = 0.15
				production_speed_buildings_factor = -0.1
				min_export = 0.4
			}
		}
		AMS_triads = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea AMS_triads"}
			picture = AMS_triad_existence
			modifier = {
				political_power_gain = -0.05
				army_core_defence_factor = -0.1
				stability_factor = -0.1
			}
		}
	}
}